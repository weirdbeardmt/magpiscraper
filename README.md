# MagPi Scraper

Simple demonstration of basic web scraping in Python using the Requests library. It's designed to run on a Raspberry Pi but it should be fine in any Python environment.

The scenario is to monitor the excellent MagPi magazine and download new PDF editions when they're available.

There are some corresponding blog articles to go with it. The [latest is here](https://www.matt-thornton.net/general/basic-web-scraping-with-python-episode-3).

## Features

* Simple web scraping in Python 2/3 using Requestes and Beautiful Soup.
* Intelligent check to only download new issues
* Demo concepts such as config import, modular design, logging, functions
* Dropbox upload integration

## Pre-requisites

* Original version was Python 2. [Latest version](https://www.matt-thornton.net/general/basic-web-scraping-with-python-episode-3) is Python 3.
* Environment setup / config is in the [original article](https://www.matt-thornton.net/tech/raspberrypi/basic-web-scraping-with-a-raspberry-pi-python-and-requests)
* Requires Requests, BeautifulSoup and lxml
* If you want to upload to Dropbox, you'll need the [Dropbox-Uploader](https://github.com/andreafabrizi/Dropbox-Uploader).

## Getting started

* Configure your environment
* [Setup Dropbox-Uploader](https://learn.adafruit.com/diy-wifi-raspberry-pi-touch-cam/dropbox-setup) including your Dropbox app and keys, etc.
* Clone the code.
* Rename config_tpl.py to config.py
* Configure the paths in config.py (this includes the path to Dropbox-Uploader shell script, if you want to use it.)
* SFTP it to your Pi (if necessary)
* Create an Output folder.

## Environment config

```python
pip3 install Requests
pip3 install bs4
pip3 install lxml
```

## Initial usage
You probably want to test it's all working before you hammer the MagPi website to download the entire back catalogue. In the latest.txt file therefore, set the value to e.g., 91. When you're ready to download the lot, set this value to 1.

## Usage

```python
python3 magpi.py
```