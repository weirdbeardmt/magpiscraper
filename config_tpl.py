# User config
UploadToDropbox = True
LatestFileName = 'latest.txt'

# Paths
PathToOutput = '/full/path/to/where/to/download/files'
PathToDropbox = '/full/path/to/dropbox-uploader/script'

# MagPi web config
RootUrl = 'https://magpi.raspberrypi.org'
def GetIssuePageUrl(page):
    return '{}/issues?page={}'.format(RootUrl, page)

def GetDownloadPageUrl(url):
    return '{}/{}'.format(RootUrl, url)